    .set noreorder
    .data

    .text
    .globl main
    .ent main

#int main()
#{
main:

	# int score = 84; 
	addi $s0, $0, 84	# s0 = 84

	# int grade;
	# if (score >= 90)
 	addi $t0, $0, 90	# t0 = 90
 	slt $t1, $t0, $s0	# t1 = (t0 < s0)
 	beq $t1, $0, else_1	# if t1 = 0, jump to else_1
	nop
	# grade = 4;
	addi $a0, $0, 4		# a0 = 4
	j exit			# jump to exit
	nop

	# else if (score >= 80)
 	else_1:			# else_1 line declared
	addi $t0, $0, 80	# t0 = 80
	slt $t1, $t0, $s0 	# t1 = t0 < s0 
	beq $t1, $0, else_2	# if t1 = 0, jump to else_2
	nop
	# grade = 3;
	addi $a0, $0, 3		# a0 = 3
	j exit			# jump to exit
	nop

	else_2:			# else_2 line declared
	# else if (score >= 70)
 	addi $t0, $0, 70	# t0 = 70
 	slt $t1, $t0, $s0	# t1 = t0 < s0
	beq $t1, $0, else_3	# if t1 = 0, jump to else 3
	nop
	# grade = 2;
	addi $a0, $0, 2		# a0 = 2
	j exit			# jump to exit
	nop
 	
	# else
	else_3:			# else_3 line declared
	# grade = 0;
	addi $a0, $0, 0		# a0 = 0
	j exit			# jump to exit
	nop

	exit:			# exit line declared
	ori $v0, $0, 20		# v0 = 20
	# PRINT_HEX_DEC(grade);
	syscall 		# print
 
	# EXIT;
        ori $v0, $0, 10    	#exit
        syscall
#}
    .end main
