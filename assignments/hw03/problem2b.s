    .set noreorder
    .data

    .text
    .globl main
    .ent main

#void print(int a)
#{
print:
	
	addi $sp, $sp, -4	# increment stack
	sw $v0, 0($sp)		# save original value of $v0

	# // should be implemented with syscall 20
	ori $v0, $0, 20		# v0 = 20
	syscall			# syscall
	lw $v0, 0($sp)		# reload original value of $v0
	addi $sp, $sp, 4	# deincrement stack
	jr $ra			# jump back to call
	nop			
#}

#int sum3(int a, int b, int c)
#{
sum3:
	# return a+b+c;
	addi $v0, $0, 0		# make sure $v0 = 0
	add $v0, $a0, $a1	# $v0 = $a0 + a1
	add $v0, $v0, $a2	# v0 = v0 + a2
	jr $ra			# jump back to call
	nop	
#}


#int polynomial(int a, int b, int c, int d, int e)
#{
polynomial:

	# int x;	
	# int y;
	# int z;
	addi $sp, $sp, -24	# increment stack
	sw $ra, 0($sp)		# store original registers
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $a2, 12($sp)
	sw $t0, 16($sp)
	sw $t1, 20($sp)
	
	# x = a << b;
	sll $t0, $a0, $a1	# t0 = a0 << a1
	# y = c << d;
	sll $t1, $a2, $a3	# t1 = a2 << a3

	add $a0, $t0, $0	# a0 = t0 
	add $a1, $t1, $0	# a1 = t1
	add $a2, $s0, $0	# a2 = s0
	# z = sum3(x, y, e);
	jal sum3		# ra = here, call sum3, v0 = sum3
	nop

	# print(x);
	jal print		# ra = here, call print
	nop
	
	# print(y);
	add $a0, $a1, $0	# a0 = a1
	jal print		# ra = here, call print
	nop

	# print(z);
	add $a0, $v0, $0	# a0 = v0
	jal print		# ra = here, call print
	nop

	# return z;
	lw $ra, 0($sp)		# reload original registers
	lw $a0, 4($sp)
        lw $a1, 8($sp)
        lw $a2, 12($sp)
	lw $t0, 16($sp)
        lw $t1, 20($sp)
	addi $sp, $sp, 24	# deincrement stack
	jr $ra			# jump to original ra
        nop        		
#}

#int main()
#{
main:

	addi $sp, $sp, -24	# increment stack
	sw $ra, 0($sp)          # store original registers
        sw $a0, 4($sp)
        sw $a1, 8($sp)
        sw $a2, 12($sp)
        sw $a3, 16($sp)
        sw $s0, 20($sp)

	# int a = 2;
	addi $a0, $0, 2		# a0 = 2	
	addi $a1, $0, 3		# a1 = 3
	addi $a2, $0, 4		# a2 = 4
	addi $a3, $0, 5		# a3 = 5
	addi $s0, $0, 6		# s0 = 6
			
	# int f = polynomial(a, 3, 4, 5, 6);
	jal polynomial		# ra = here, v0 = polynomial
	nop
	
	# print(a);
	jal print		# ra = here, call print	
	nop		

	# print(f);
	add $a0, $v0, $0	# a0 = v0
	jal print		# ra = here, call print
	nop

	lw $ra, 0($sp)		# reload original registers
	lw $a0, 4($sp)	
	lw $a1, 8($sp)
	lw $a2, 12($sp)
	lw $a3, 16($sp)
	lw $s0, 20($sp)
	addi $sp, $sp, 24	# deincrement stack
	
        jr $ra			# jump to original ra
	nop
#}

    .end main
