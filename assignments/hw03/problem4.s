    .set noreorder
    .data

    .text
    .globl main
    .ent main

#typedef struct record {
# int field0;
# int field1;
#} record;

#int main()
#{
main:

	# record *r = (record *) MALLOC(sizeof(record));	
	addi $a0, $0, 8		# a0 = 8
	ori $v0, $0, 9		# v0 = 9
	syscall			# malloc call
	add $s0, $0, $v0	# s0 = v0

	# r->field0 = 100;
	addi $t0, $0, 100	# t0 = 100	
	sw $t0, 0($s0)		# *s0 = t0

	# r->field1 = -1;
	addi $t0, $0, -1	# t0 = -1
	sw $t0, 4($s0)		# *s0 = t0

	# EXIT;
        ori $v0, $0, 10     	# v0 = 10
        syscall			# exit
#}
    .end main
