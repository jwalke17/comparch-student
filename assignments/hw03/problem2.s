    .set noreorder
    .data
    A: .byte 1,25,7,9,-1

    .text
    .globl main
    .ent main
# int main()
# {
main:
	# int i;
	# int current;
	# int max;	
	# i = 0;		
	addi $s0, $0, 0		# s0 = 0
	la $s3, A		# s3 = &A
	# current = A[0];
	lb $s1, 0($s3)		# s1 = *(s3)
	# max = 0;
	addi $s2, $0, 0		# s2 = 0

	# while (current > 0) {
	sgt $t0, $s1, $0	# t0 = s1 > 0
	beq $t0, $0, exit	# if t0 = 0, jump to exit
	nop
	while:			# while line declared
	# if (current > max)
	sgt $t0, $s1, $s2	# t0 = s1 > s2
	beq $t0, $0, else	# if t0 = 0, jump to else
	nop
	# max = current;
	add $s2, $0, $s1	# s2 = s1
	
	else:			# else line declared here
	# i = i + 1;
	addi $s0, $s0, 1	# s0 = s0 + 1
	addi $s3, $s3, 1	# s3 = s3 + 1

	# current = A[i];
	lb $s1, 0($s3)		# s1 = *(s3)

 	sgt $t0, $s1, $0	# t0 = s1 > 0
	bne $t0, $0, while	# if t != 0, jump to while
	nop
	# }

	exit:			# exit line declared here
	# PRINT_HEX_DEC(max);
	add $a0, $0, $s2	# a0 = s2
	ori $v0, $0, 20		# print hex/dec
	syscall

	# EXIT;
        ori $v0, $0, 10     	# exit
        syscall
#}
    .end main
