    .set noreorder
    .data

    .text
    .globl main
    .ent main


#void print(int a)
#{
print:
	addi $sp, $sp, -4	# added 4 bytes of stack data
	sw $v0, 0($sp)		# stored $v0 in first 4 bytes

	# // should be implemented with syscall 20

	ori $v0, $0, 20		# $v0 = 20
	syscall			# print hex
	lw $v0, 0($sp)		# reload original value of $v0
	addi $sp, $sp, 4	# deincrement stack
	jr $ra			# return to call
	nop

	#}

#int main()
#{

main:

	addi $sp, $sp, -60	# increment stack by 28 bytes
	sw $ra, 0($sp)		# store values that will change in main
	sw $v0, 4($sp)
	sw $s0, 8($sp)
	sw $s1, 12($sp)
	sw $t0, 16($sp)
	sw $t1, 20($sp)
	sw $t2, 24($sp)

	# int A[8];
	# int i;
		
	add $s0, $sp, 28	# $s0 = address of A

	# A[0] = 0;		
	addi $t0, $0, 0		# $t0 = 0
	sw $t0, 0($s0)		# store $t0 into first bytes of A
	
	# A[1] = 1;
	addi $t0, $0, 1		# $t0 = 1
	sw $t0, 4($s0)		# store $t0 into 4-7 bytes of A
	
	# for (i = 2; i < 8; i++) {	
	addi $s1, $0, 2		# $s1 = 2
	for:			# for declared line

	# A[i] = A[i-1] + A[i-2];
	lw $t0, 0($s0)		# $t0 = 0-3 of $s0 address
	lw $t1, 4($s0)		# $t1 = 4-7 of $s0 address
	add $t2, $t1, $t0	# $t2 = $t1 + $t0
	sw $t2, 8($s0)		# 8-11 of $s0 address = $t2
	
	# print(A[i]);
	lw $a0, 8($s0)		# $a0 = 8-11 of $s0 address		
	jal print		# $ra = here, call print
	nop

	slti $t0, $s1, 8	# $t0 = $s1 < 8
	beq $t0, $0, for_out	# if $t0 == 0, jump out of loop
	nop
	addi $s1, $s1, 1	# else, i = i + 1
	addi $s0, $s0, 4	# and $s0 = 4($s0)
	j for			# jump to beginning of loop
	nop
	
	# }
	for_out:		# out of loop

	lw $ra, 0($sp)		# reload original values
        lw $v0, 4($sp)
        lw $s0, 8($sp)
        lw $s1, 12($sp)
        lw $t0, 16($sp)
        lw $t1, 20($sp)
        lw $t2, 24($sp)
				
	addi $sp, $sp, 60	# deincrement stack
	jr $ra			# return
	nop
	#}

    .end main
