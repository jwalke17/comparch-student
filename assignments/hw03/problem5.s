    .set noreorder
    .data

    .text
    .globl main
    .ent main

#typedef struct elt {
# int value;
# struct elt *next;
#} elt;

#int main()
#{

main:

	# elt *head;	
	# elt *newelt;
	ori $v0, $0, 9		# v0 = 9
	li $a0, 8		# a0 = 8
	# newelt = (elt *) MALLOC(sizeof (elt));
	syscall			# malloc
	
	# newelt->value = 1;
	li $t0, 1		# t0 = 1
	sw $t0, 0($v0)		# *(v0) = t0
	# newelt->next = 0;
	li $t0, 0		# t0 = 0	
	sw $t0, 4($v0)		# *(v0 + 4)

	# head = newelt;
	la $s0, 0($v0)		# s0 = v0
	
	# newelt = (elt *) MALLOC(sizeof (elt));
	ori $v0, $0, 9		# v0 = 9
	li $a0, 8		# a0 = 8
	syscall			# malloc
	
	# newelt->value = 2;
	li $t0, 2		# t0 = 2
	sw $t0, 0($v0)		# *(v0) = t0
	# newelt->next = head;
	sw $s0, 4($v0)		# *(v0+4) = s0
	
	# head = newelt;
	la $s0, 0($v0)		# s0 = v0

	# PRINT_HEX_DEC(head->value);
	ori $v0, $0, 20		# v0 = 20
	lw $a0, 0($s0)		# a0 = *(s0)
	syscall			# print

	# PRINT_HEX_DEC(head->next->value);
	la $t0, 4($s0)		# t0 = (s0 + 4)
	lw $a0, 0($t0)		# a0 = *(t0)
	syscall			# print
	
	# EXIT;
        ori $v0, $0, 10     	# exit
        syscall			
#}
    .end main
