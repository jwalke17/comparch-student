    .set noreorder
    .data
#int A[8]
    A: .space 32
    .text
    .globl main
    .ent main

#int main()
#{
main:
	# int i;
	la $s0, A		# s0 = A

	# A[0] = 0;
	addi $t0, $0, 0		# t0 = 0	
	sw $t0, 0($s0)		# *(s0+0) = t0
	nop
	# A[1] = 1;
	addi $t0, $0, 1		# t0 = 1
	sw $t0, 4($s0)		# *(s0+4) = t0
	nop

	# for (i = 2; i < 8; i++) {
	addi $s3, $0, 2		# s3 = 2
	for:			# for line declared

	# A[i] = A[i-1] + A[i-2];
	lw $t0, 0($s0)		# t0 = *(s0)
	nop
	lw $t1, 4($s0)		# t1 = *(s0+4)
	nop
	add $t2, $t0, $t1	# t2 = t1 + t0
	sw $t2, 8($s0)		# *(s0 + 8) = t2
	nop

	# PRINT_HEX_DEC(A[i]);
	lw $a0, 8($s0)		# a0 = *(s0+8)
	nop
	ori $v0, $0, 20		# print hex/dec
	syscall


	slti $t1, $s3, 8	# t1 = s3 < 8
	beq $t1, $0, for_out	# if t1 = 0, leave loop
	nop
	addi $s3, $s3, 1	# s3 = s3 + 1
	addi $s0, $s0, 4	# s0 = s0 + 4
	j for			# jump to for
	nop
	# }

	
	for_out:		# for_out line declared

	# EXIT;
        ori $v0, $0, 10  	# exit
        syscall			
#}
    .end main
